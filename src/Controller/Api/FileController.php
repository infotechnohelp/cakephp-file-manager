<?php

namespace FileManager\Controller\Api;

use Cake\ORM\TableRegistry;
use FileManager\Exceptions\MyPluginException;

/**
 * Class MyPluginController
 * @package FileManager\Controller\Api
 */
class FileController extends AppController
{

    public $autoRender = false;

    public function upload(string $dirPath = null)
    {
        $this->request->allowMethod('post');

        $targetDirPath = $dirPath ?? 'uploads/file-manager';

        $targetDirs = explode('/', $targetDirPath);

        $compoundPath = '';

        foreach ($targetDirs as $targetDir) {
            $compoundPath .= $targetDir . DS;
            if (! file_exists($compoundPath)) {
                mkdir($compoundPath);
            }
        }

        $fileName = basename($_FILES["fileUpload"]["name"]);

        $FilesTable = TableRegistry::getTableLocator()->get('FileManager.Files');

        $LastFile   = $FilesTable->find()->last();
        $nextFileId = ($LastFile !== null) ? $LastFile->get('id') + 1 : 1;

        $calculatedTargetFilePath = sprintf("%s%s.%s", $compoundPath, $nextFileId, $fileName);

        if (file_exists($calculatedTargetFilePath) && $FilesTable->findById($nextFileId)->first() === null) {
            unlink($calculatedTargetFilePath);
        }

        $uploadResult = move_uploaded_file($_FILES["fileUpload"]["tmp_name"], $calculatedTargetFilePath);

        if (! $uploadResult) {
            return $this->response->withStringBody(0);
        }

        $File = $FilesTable->saveOrFail($FilesTable->newEntity([
            'title'    => $fileName,
            'dir_path' => $targetDirPath,
        ]));

        if ($File->get('id') !== $nextFileId) {
            $actualTargetFilePath = sprintf("%s%s.%s", $compoundPath, $File->get('id'), $fileName);
            rename($calculatedTargetFilePath, $actualTargetFilePath);
        }

        $url = $actualTargetFilePath ?? $calculatedTargetFilePath;

        return $this->response->withStringBody(json_encode([
            'data'    => $File->set('url', str_replace(DS, '/', $url)),
            'message' => null,
        ]));
    }

    public function delete(int $id)
    {
        $this->request->allowMethod('post');

        $FilesTable = TableRegistry::getTableLocator()->get('FileManager.Files');

        $File = $FilesTable->findById($id)->first();

        if ($File === null) {
            return $this->response->withStringBody(json_encode([
                'data'    => null,
                'message' => 'File cannot be found from the database',
            ]));
        }

        $filePath = sprintf("%s/%s.%s", $File->get('dir_path'), $File->get('id'), $File->get('title'));

        if (!unlink($filePath)) {
            return $this->response->withStringBody(json_encode([
                'data'    => null,
                'message' => 'File cannot be found from the file system',
            ]));
        }

        $FilesTable->deleteOrFail($File);

        return $this->response->withStringBody(json_encode([
            'data'    => true,
            'message' => null,
        ]));
    }
}
